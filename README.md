# CRUD

An opinionated base for CRUD ops in your Rails app. Provides a
base CrudController and view templates you can use to quickly build out
admin sections.

__Build up, not down!__

This engine uses:

- Kaminari for pagination
- SimpleForm and NestedForms for forms
- FontAwesome for awesome font icons
- Sass to compile CSS

## Installation

Add this line to your gemfile

```
gem 'crud'
```

By default, the `CrudController` will extend your
`ApplicationController`.

If you need it to extend a different controller, then add an initializer
here: `/config/intializers/crud.rb` and set the parent_controller
option:

```
Crud.setup do|config|
  config.parent_controller = "MyController"
end
```

## License

This project uses the MIT-LICENSE.
