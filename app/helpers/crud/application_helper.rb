module Crud
  module ApplicationHelper

    def title_from_controller(str)
      @title_from_controller ||= str.to_s.split('/').last.singularize.titleize
    end

    def flash_messages(opts={})
      defaults = {
        id: 'flash-messages',
      }
      opts = defaults.merge(opts)
      return if flash.empty?
      content_tag(:div, opts) do
        flash.map do |type, messages|
          messages = [messages] unless messages.is_a?(Array)
          messages.map do |message|
            content_tag(:div, message, class: "alert alert-#{type}") do
              (link_to('&times;'.html_safe, '#', class: 'close', 'data-dismiss' => 'alert') + ' ' + message.html_safe).html_safe
            end
          end
        end.join("\n").html_safe
      end
    end

    def link_to_add_fields(name, f, type)
      new_object = f.object.send "build_#{type}"
      id = "new_#{type}"
      fields = f.send("#{type}_fields", new_object, child_index: id) do |builder|
        render(type.to_s + "_fields", f: builder)
      end
      link_to(name, '#', class: "add-fields small button", data: {id: id, fields: fields.gsub("\n", "")})
    end

    def remove_link(f, name='Remove <i class="icon-trash"></i>', opts={})
      defaults = {
        class: 'button small remove'
      }
      opts = defaults.merge(opts)
      f.link_to_remove name.html_safe, opts
    end


    def crud_link(opts, defaults)
      opts = defaults.merge(opts)
      link_to opts.delete(:name).html_safe, opts.delete(:url), opts
    end

    def back_link(opts={})
      defaults = {
        name: '<i class="icon-caret-left"></i> Back',
        url: url_for(action: :index),
        class: 'button small nav',
      }
      # if session[:crud_back] is set (see crud_controller#index), and
      # :skip_session was not passed in, then go there instead
      if !opts.delete(:skip_session) && session[:crud_back]
        opts[:url] = session[:crud_back]
      end
      crud_link opts, defaults
    end

    def edit_link(obj, opts={})
      return if params[:action] == 'edit' || obj.new_record?
      defaults = {
        name: 'Edit',
        url: url_for(action: :edit, id: obj.to_param),
        class: 'button small edit',
      }
      crud_link opts, defaults
    end

    def confirm_destroy_link(obj, opts={})
      return if params[:action] == 'confirm_destroy' || obj.new_record?
      defaults = {
        name: 'Delete',
        url: url_for(action: :confirm_destroy, id: obj.to_param),
        class: 'button small delete',
      }
      crud_link opts, defaults
    end

  end
end
