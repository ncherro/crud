module Crud
  module ErrorMessagesHelper
    # Render error messages for the given objects. The :message and
    # :header_message options are allowed.
    def error_messages_for(*objects)
      options = objects.extract_options!
      options[:header_message] ||= "Invalid Fields"
      options[:message] ||= "Correct the following errors and try again."
      messages = objects.compact.map { |o| o.errors.full_messages }.flatten
      unless messages.empty?
        content_tag(:div, :class => "error-messages") do
          list_items = messages.map { |msg| content_tag(:li, msg) }
          r = ''
          r << content_tag(:h3, options[:header_message]) unless options[:header_message].blank?
          r << content_tag(:p, options[:message]) unless options[:message].blank?
          r << content_tag(:ul, list_items.join.html_safe)
          r.html_safe
        end
      end
    end
    module FormBuilderAdditions
      def error_messages(options = {})
        @template.error_messages_for(@object, options)
      end
    end
  end
end
ActionView::Helpers::FormBuilder.send(:include, Crud::ErrorMessagesHelper::FormBuilderAdditions)
