Rails.application.routes.draw do

  namespace :admin do
    resources :pages do
      member do
        get 'confirm_destroy'
      end
    end
  end

  resources :pages, :only => [:index,]

  root :to => 'pages#index'

end
