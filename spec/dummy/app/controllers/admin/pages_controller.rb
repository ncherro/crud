class Admin::PagesController < Admin::BaseController

  private
  def params_key
    :page
  end

  def resource_model
    Page
  end

end
