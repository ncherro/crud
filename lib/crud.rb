require "crud/engine"

module Crud

  # The parent controller the Crud controller inherits from.
  # Defaults to ApplicationController.  This should be set early
  # in the initialization process, and should be a string
  mattr_accessor :parent_controller
  @@parent_controller = "ApplicationController"

  def self.setup
    yield self
  end

end
