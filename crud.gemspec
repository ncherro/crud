$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "crud/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "crud"
  s.version     = Crud::VERSION
  s.authors     = ["Nick Herro"]
  s.email       = ["ncherro@gmail.com"]
  s.homepage    = "http://www.ncherro.com"
  s.summary     = "Provides base controllers, views, and helpers for a quick admin setup"
  s.description = "Uses Rails 3.1+ view inheritance to allow for quick and flexible CMS templating"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", ">= 4.0.0"
  # s.add_dependency "jquery-rails"

  s.add_dependency "country_select"
  s.add_dependency "kaminari", ">= 0.14"
  s.add_dependency "nested_form", ">= 0.3"
  s.add_dependency "simple_form"
  s.add_dependency "sass-rails", "~> 4.0.0.rc2"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'capybara'
  s.add_development_dependency 'factory_girl_rails'
end
